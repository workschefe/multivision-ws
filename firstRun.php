<?php
    /*** This file create all the database need, if it runs for the second time, it quits because already have the db. */
    $host="localhost"; 
    $user="root"; 
    $pass=""; 
    $db="multivision"; 

    try {
        $conn = new PDO("mysql:host=$host", $user, $pass);
        $conn->exec("CREATE DATABASE `$db`;
                CREATE USER '$user'@'localhost' IDENTIFIED BY '$pass';
                GRANT ALL ON `$db`.* TO '$user'@'localhost';
                FLUSH PRIVILEGES;") 
        or die(print_r($conn->errorInfo(), true));
        $conn = null;

        $conn = new PDO("mysql:host=".$host.";dbname=".$db, $user, $pass);
        $conn->exec("CREATE TABLE `users` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `username` varchar(255) NOT NULL,
            `password` blob NOT NULL,
            `email` varchar(255) NOT NULL,
            `status` tinyint(1) NOT NULL,
            `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            
            INSERT INTO `users` (`id`, `username`, `password`, `email`, `status`) VALUES
            (0, 'helloprint', 'P@ssw0rd!', 'xxxxxx@xxxxxx.xxx', 1),
            (0, '111', '111', '1111@asd.com', 0);") 
        or die(print_r($conn->errorInfo(), true));
    } catch (PDOException $e) {
        die("DB ERROR: ". $e->getMessage());
    }
?>