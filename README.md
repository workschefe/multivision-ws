# Multivision Challange - Distributed system

## How to run?

Very simple, just clone to your **xampp** or **lampp** localhost directory, run the file "*firstRun.php*" to add the db, table and data and go to index.php, no dependencies.

In linux you must have
php5.6
php5.6-curl
php5.6-mysql
php5.6-pdo

ex: sudo apt-get install php5.6

ps: if you have dificulties installing some libs (cant find repository ex) try this:
sudo add-apt-repository -y ppa:ondrej/php
sudo apt update


The project is divided in 2 parts

**client** -> **root** files & **includes**, which is divided by **views** & **controller**;  
**server** -> **api** & **db** stuff;

ps2: because i dont use external libs (ex. using composer) you may have to configure your localhost to send mail.

any question just ask me :)