<body id="page-top">
    <div id="container">
        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800" style="padding: 10px;">Multivision</h1>
            </div>

            <div class="row">
                <div class="col-xl-12 col-md-12 text-center">
                    <div class="card border-left-primary shado">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <form class="col-xl-12" action="index.php" method="POST">
                                    <div class="col-xl-12">
                                        <h4> Account Login </h4>
                                    </div>
                                    <div class="col-xl-12 col-md-12" style="padding: 10px;">
                                        <input type="text" name="user" class="form-control form-control-user" placeholder="Enter Username" required>
                                    </div>
                                    <div class="col-xl-12 col-md-12" style="padding: 10px;">
                                        <input type="password" name="pass" class="form-control form-control-user" placeholder="Enter Password" required>
                                    </div>
                                    <span style="color: red; font-weight: bolder;"> <?php echo $error; ?></span>
                                    <span style="color: green; font-weight: bolder;"> <?php echo $ok; ?></span>
                                    <div class="col-xl-12 col-md-12" style="padding: 10px;">
                                        <button type="submit" class="btn btn-primary" type="button" aria-expanded="false"> Login </button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                    Forgot your Password?
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<div class="modal" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Password Recovery</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="index.php">
                <div class="modal-body">
                    <input type="text" class="form-control form-control-user" name="userRec" id="userRec" placeholder="Username" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Send Email Request</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('#myModal').on('shown.bs.modal', function() {
        $('#myInput').trigger('focus')
    })
</script>