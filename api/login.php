<?php
    use Multivision\API\User\UserClass as user;

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    
    header("Access-Control-Allow-Credentials: true");
    header('Content-Type: application/json;charset=utf-8');

    if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW'])) {
        header("WWW-Authenticate: Basic realm=\" Multivision \"");
        exit(header("HTTP\ 1.0 401 Unauthorized"));
    } else {    
        require_once('includes.php');
        if (isset($_POST['body'])) {        
            $body = json_decode($_POST['body']);    

            $user = (!empty($body->user)) ? filter_var($body->user, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH) : die() ;
            $pass = (!empty($body->pass)) ? filter_var($body->pass, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH) : die() ;
    
            if (!empty($user) && !empty($pass)) {
                echo json_encode(user::login($user, $pass));
            } else {
                header("HTTP\ 1.0 406 Not Acceptable");
                exit();
            }
        } else {
        exit(header("HTTP\ 1.0 403 Forbidden"));
        }
    }