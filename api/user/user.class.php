<?php namespace Multivision\API\User;
    use PDO;
    require_once ('includes.php');

    use Multivision\Api\Core\DB\DatabaseClass as db;
    
    class UserClass {

        public static function login($user, $pass) {
            $conn = db::bdConn();

            $query = " SELECT u.id as id, u.username as user, u.email as email, u.status as status FROM users u
                        WHERE
                            u.username = ? 
                        AND
                            u.password = ?
                        LIMIT 
                            1 ";
            $stmt = $conn->prepare( $query );
            $stmt->bindParam(1, $user, PDO::PARAM_STR);
            $stmt->bindParam(2, $pass, PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!$row) {
                return false;
            } else {
                return $row;
            }
        }

        public static function getUser($user) {
            $conn = db::bdConn();

            $query = " SELECT u.username as user, u.email as email, u.password as pass FROM users u
                        WHERE
                            u.username = ?
                        LIMIT 
                            1 ";
            $stmt = $conn->prepare( $query );
            $stmt->bindParam(1, $user, PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!$row) {
                return false;
            } else {
                return $row;
            }
        }
    }