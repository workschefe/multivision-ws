<?php namespace Multivision\Api\Core\DB;
    use PDO;

    class DatabaseClass {
    
        public static function bdConn() {
            $host = "localhost";
            $db_name = "multivision";
            $username = "root";
            $password = "";

            try {
                $conn = new PDO("mysql:host=" .$host . ";dbname=" .$db_name,$username,$password);
                
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $conn->exec("set names utf8");
            } catch(PDOException $exception){
                echo "Connection error: " . $exception->getMessage();
            }
            return $conn;
        }
    }
?>