<?php namespace Multivision\Call;

    class CallClass {

        private $cURL;

        public function __construct() {
            $this->cURLconfig();
        }

        private function cURLconfig() {
            $cURLuser = 'lol';
            $cURLpass = 'lole';

            $curl = curl_init(); 
            if (!$curl) {
                die("Couldn't initialize a cURL handle"); 
            }

            curl_setopt($curl, CURLOPT_USERPWD, $cURLuser.':'.$cURLpass);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
            curl_setopt($curl, CURLOPT_FAILONERROR, true); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($curl, CURLOPT_TIMEOUT, 50);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 

            $this->cURL = $curl;
            return $curl;
        }

        public function callAPILogin($data = false) {

            $curl = (!$this->cURL) ? $this->cURLconfig() : $this->cURL;
            $cURLurl  = 'http://localhost/multivision/api/login.php';
            curl_setopt($curl, CURLOPT_URL, $cURLurl);
            curl_setopt($curl, CURLOPT_POSTFIELDS, "body=".$data);
            $res = curl_exec($curl); 
            if (curl_errno($curl)) {
                curl_close($curl);
                return false;
            } else {
                curl_close($curl);
                $res = json_decode($res);
                return $res;
            }
        }

        public function callAPIUser($data = false) {

            $curl = (!$this->cURL) ? $this->cURLconfig() : $this->cURL;
            $cURLurl  = 'http://localhost/multivision/api/user.php';
            curl_setopt($curl, CURLOPT_URL, $cURLurl);
            curl_setopt($curl, CURLOPT_POSTFIELDS, "body=".$data);
            $res = curl_exec($curl); 
            if (curl_errno($curl)) {
                curl_close($curl);
                return false;
            } else {
                curl_close($curl);
                $res = json_decode($res);
                return $res;
            }
        }
    }

?>