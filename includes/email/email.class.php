<?php namespace Multivision\Email;
ini_set("SMTP","ssl://smtp.gmail.com");
ini_set("smtp_port","465");


class EmailClass {

    private static $templateRecovery = '
        <html>
            <head>
                <title>Password Recovery!</title>
            </head>
            <body>
                <h4>Recover Password</p>
                <p>In response to your request, here is your password: _PASSWORD_ </p>
            </body>
        </html>
    ';

    private static function getTemplate($no) {
        switch ($no) {
            case 1: return array('Message' => self::$templateRecovery, 'Subject' => 'Password Recovery!');
            default: return false;
        }
    }

    public static function sendEmail($data, $templateNo) {

        $options = self::getTemplate($templateNo);
        if (!$options) {
            return false;
        }

        $subject = $options['Subject'];
        $message = $options['Message'];
        $message = str_replace("_PASSWORD_", $data->pass, $message);
        
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'To: '.$data->user.' <'.$data->email.'>' . "\r\n";
        $headers .= 'From: Multivision <multivision@lol.com>' . "\r\n";
        
        $mail = mail($data->email, $subject, $message, $headers);
        return $mail;
    }

 
}