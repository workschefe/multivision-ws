<?php namespace Multivision\User\PassRecovery;

    use Multivision\Call\CallClass as cURL;
    use Multivision\Email\EmailClass as Email;

    class PassRecoveryClass {

        public static function getUserRecover($user) {
            $data = '{ "user": "'.$user.'" }';
            $lg = new cURL();
            $res = $lg->callAPIUser($data);

            if (!$res) {
                return false;
            } else {
                return Email::sendEmail($res, 1);
            }

        }
    }
?>
