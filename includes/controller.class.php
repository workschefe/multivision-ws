<?php
    require_once ('inc.php');

    use Multivision\User\Login\LoginClass as Lg;
    use Multivision\User\PassRecovery\PassRecoveryClass as Pw;

    if (isset($_POST['user']) && isset($_POST['pass'])) {
        $user = $_POST['user'];
        $pass = $_POST['pass'];

        if (empty($user) || empty($pass)) {
            $error = 'Wrong Username or Password.';
        } else {
            $res = Lg::login($user, $pass);
            if (!$res) {
                $error = 'Wrong Username or Password.';
                $userData = null;
            } else {
                $error = null;
                $userData = $res;
                $userData->status = ($userData->status) ? 'Active' : 'Inactive';
            }
        }
    }

    if (isset($_POST['userRec'])) {
        $userRec = $_POST['userRec'];

        if (empty($userRec)) {
            $errorEmail = 'Wrong Username please try again.';
        } else {
            $res = Pw::getUserRecover($userRec);
            if (!$res) {
                $error = 'Wrong or unknown Username please try again.';
                $userData = null;
            } else {
                $error = null;
                $userData = null;
                $ok = 'Email Sent.';
            }
        }
    }
?>