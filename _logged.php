<body id="page-top">
    <div id="container" style="padding: 20px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-md-12 text-center">
                    <div class="card border-left-primary shado">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col-sm-12" style="padding: 10px;">
                                    <h2> Wellcome <i><?php echo $userData->user; ?></i> </h2>
                                </div>
                                <div class="col-sm-3" style="padding: 10px;">
                                    <h6>Email: <i><?php echo $userData->email; ?></i></h6>
                                </div>
                                <div class="col-sm-3" style="padding: 10px;">
                                    <h6>Status: <i><?php echo $userData->status ?></i></h6>
                                </div>
                                <div class="col-sm-12" style="padding: 10px;">
                                    <a type="submit" href="index.php" name="logout" class="btn btn-danger" type="button" aria-expanded="false"> Logout  </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>